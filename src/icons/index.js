import { vec4, vec3, quat, mat4, vec2 } from "gl-matrix";
import palette from "./textures/palette.png";
import diffuse from "./textures/diffuse.png";
import glossy from "./textures/glossy.png";
import circle from "./geometries/circle.data?url";
import css from "./geometries/css.data?url";
import js from "./geometries/js.data?url";
import bash from "./geometries/bash.data?url";
import blender from "./geometries/blender.data?url";
import figma from "./geometries/figma.data?url";
import aeropress from "./geometries/aeropress.data?url";
import arch from "./geometries/arch.data?url";
import controller from "./geometries/controller.data?url";
import keyboard from "./geometries/keyboard.data?url";
import grinder from "./geometries/grinder.data?url";
import vim from "./geometries/vim.data?url";
import shadedVertex from "./shaders/shaded.vert?raw";
import shadedFragment from "./shaders/shaded.frag?raw";
import shadowVertex from "./shaders/shadow.vert?raw";
import shadowFragment from "./shaders/shadow.frag?raw";
import reflectionVertex from "./shaders/reflection.vert?raw";
import reflectionFragment from "./shaders/reflection.frag?raw";

const bodyElement = document.body;
const mainElement = document.querySelector("main");
const canvasElement = document.querySelector("canvas");
const iconScaleElement = document.querySelector(".icon-scale");

canvasElement.onclick = () => {
  canvasElement.toBlob((blob) => {
    const url = URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.href = url;
    link.innerText = "Download";
    bodyElement.appendChild(link);
  });
};

const gl = canvasElement.getContext("webgl2");

const icons = [
  { geometryUrl: css },
  { geometryUrl: js },
  { geometryUrl: blender },
  { geometryUrl: figma },
  { geometryUrl: aeropress },
  { geometryUrl: arch },
  { geometryUrl: controller },
  { geometryUrl: keyboard },
  { geometryUrl: grinder },
  { geometryUrl: bash },
  { geometryUrl: vim },
];
const loadedIcons = [];
let loadedIconsCount = 0;

const appearDuration = 1000;

const shadedProgram = createProgram(shadedVertex, shadedFragment);
const shadedUniforms = {
  model: gl.getUniformLocation(shadedProgram, "uModel"),
  projectionView: gl.getUniformLocation(shadedProgram, "uProjectionView"),
  paletteSampler: gl.getUniformLocation(shadedProgram, "uPaletteSampler"),
  diffuseSampler: gl.getUniformLocation(shadedProgram, "uDiffuseSampler"),
  glossySampler: gl.getUniformLocation(shadedProgram, "uGlossySampler"),
};

const reflectionShadowVOA = gl.createVertexArray();
const reflectionShadowElementArrayBuffer = gl.createBuffer();
const reflectionShadowArrayBuffer = gl.createBuffer();
const reflectionShadowIconsBuffer = gl.createBuffer();
const reflectionShadowIconData = new Float32Array(icons.length * 8);
let reflectionShadowIndexCount = 0;

const reflectionProgram = createProgram(reflectionVertex, reflectionFragment);
const reflectionUniforms = {
  projectionView: gl.getUniformLocation(reflectionProgram, "uProjectionView"),
};

const shadowProgram = createProgram(shadowVertex, shadowFragment);
const shadowUniforms = {
  projectionView: gl.getUniformLocation(shadowProgram, "uProjectionView"),
};

const projectionView = mat4.create();
const model = mat4.create();

const paletteTexture = gl.createTexture();
const diffuseTexture = gl.createTexture();
const glossyTexture = gl.createTexture();

let bodyWidth = 0;
let bodyHeight = 0;
let mainWidth = 0;
let mainHeight = 0;
let mainRadius = 0;
let iconScale = 0;
let lastTime = 0;

async function loadImage(url) {
  return new Promise((resolve) => {
    const image = new Image();
    image.addEventListener(
      "load",
      () => {
        resolve(image);
      },
      { once: true },
    );

    image.src = url;
  });
}

async function loadGeometry(url) {
  const response = await fetch(url);
  const arrayBuffer = await response.arrayBuffer();
  const dataView = new DataView(arrayBuffer);

  return {
    arrayBuffer: arrayBuffer,
    indicesOffset: dataView.getUint32(dataView.byteLength - 16, true),
    coordsOffset: dataView.getUint32(dataView.byteLength - 12, true),
    normalsOffset: dataView.getUint32(dataView.byteLength - 8, true),
    uvsOffset: dataView.getUint32(dataView.byteLength - 4, true),
    reflectionColor: vec4.fromValues(
      dataView.getFloat32(dataView.byteLength - 60, true),
      dataView.getFloat32(dataView.byteLength - 56, true),
      dataView.getFloat32(dataView.byteLength - 52, true),
      dataView.getFloat32(dataView.byteLength - 48, true),
    ),
    rotation: quat.fromValues(
      dataView.getFloat32(dataView.byteLength - 44, true),
      dataView.getFloat32(dataView.byteLength - 40, true),
      dataView.getFloat32(dataView.byteLength - 36, true),
      dataView.getFloat32(dataView.byteLength - 32, true),
    ),
    size: vec3.fromValues(
      dataView.getFloat32(dataView.byteLength - 28, true),
      dataView.getFloat32(dataView.byteLength - 24, true),
      dataView.getFloat32(dataView.byteLength - 20, true),
    ),
    indexCount:
      dataView.getUint32(dataView.byteLength - 12, true) /
      Uint16Array.BYTES_PER_ELEMENT,
  };
}

function createShader(type, source) {
  const shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);

  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    gl.getShaderInfoLog(shader);
  }

  return shader;
}

function createProgram(vertexShaderSource, fragmentShaderSource) {
  const program = gl.createProgram();

  const vertexShader = createShader(gl.VERTEX_SHADER, vertexShaderSource);
  const fragmentShader = createShader(gl.FRAGMENT_SHADER, fragmentShaderSource);

  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);

  gl.linkProgram(program);

  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    throw gl.getProgramInfoLog(program);
  }

  return program;
}

function updateProjectionView() {
  mat4.ortho(
    projectionView,
    bodyWidth * -0.5,
    bodyWidth * 0.5,
    bodyHeight * -0.5,
    bodyHeight * 0.5,
    -400,
    1,
  );
}

function updateCanvas() {
  gl.viewport(
    0,
    0,
    Math.round(bodyWidth * window.devicePixelRatio),
    Math.round(bodyHeight * window.devicePixelRatio),
  );

  canvasElement.width = Math.round(bodyWidth * window.devicePixelRatio);
  canvasElement.height = Math.round(bodyHeight * window.devicePixelRatio);

  canvasElement.style.width = `${Math.round(bodyWidth)}px`;
  canvasElement.style.height = `${Math.round(bodyHeight)}px`;
}

const mouse = vec2.create();

document.addEventListener("mousemove", (e) => {
  vec2.set(mouse, e.pageX - bodyWidth * 0.5, -e.pageY + bodyHeight * 0.5);
});

const translationNormalized = vec2.create();
const translationForce = vec2.create();
const translationNormalizedOther = vec2.create();
const translationDiffIcon = vec2.create();
const translationNormal = vec2.create();
const translationDiffMain = vec2.create();
const translationDiffBody = vec2.create();
const translationDiffMouse = vec2.create();
const rotation = quat.create();
const scale = vec3.create();

function handleAnimationFrame(time) {
  const timeDelta = (time - lastTime) * 0.001;
  lastTime = time;

  // Drop frame if delta is too high
  if (timeDelta > 1) {
    return requestAnimationFrame(handleAnimationFrame);
  }

  for (let i = 0; i < loadedIconsCount; i++) {
    const icon = loadedIcons[i];

    vec2.set(
      translationNormal,
      icon.translation[0] > 0 ? 1 : -1,
      icon.translation[1] > 0 ? 1 : -1,
    );
    vec2.multiply(translationNormalized, icon.translation, translationNormal);

    vec2.set(
      translationDiffMain,
      -mainWidth * 0.5 + translationNormalized[0] - icon.radius,
      -mainHeight * 0.5 + translationNormalized[1] - icon.radius,
    );

    if (
      translationNormalized[0] < mainRadius + icon.radius &&
      translationNormalized[1] < mainRadius + icon.radius
    ) {
      const distance = Math.min(
        0,
        vec2.length(translationNormalized) - mainRadius - icon.radius,
      );

      if (distance < 0) {
        vec2.set(
          translationForce,
          translationNormalized[0],
          translationNormalized[1],
        );
        vec2.normalize(translationForce, translationForce);
        vec2.scale(translationForce, translationForce, Math.pow(distance, 2));

        vec2.multiply(translationForce, translationForce, translationNormal);
        vec2.scaleAndAdd(
          icon.velocity,
          icon.velocity,
          translationForce,
          timeDelta * 0.0001,
        );
      }
    }

    vec2.set(
      translationDiffBody,
      bodyWidth * 0.5 - translationNormalized[0] - icon.radius * 2,
      bodyHeight * 0.5 - translationNormalized[1] - icon.radius * 2,
    );

    if (translationDiffBody[0] < 0 || translationDiffBody[1] < 0) {
      if (translationDiffBody[0] < 0 && translationDiffBody[1] < 0) {
        const distance = vec2.length(translationDiffBody);
        vec2.set(
          translationForce,
          translationDiffBody[0],
          translationDiffBody[1],
        );
        vec2.normalize(translationForce, translationForce);
        vec2.scale(translationForce, translationForce, Math.pow(distance, 2));
      } else if (translationDiffBody[0] < 0) {
        vec2.set(translationForce, -Math.pow(translationDiffBody[0], 2), 0);
      } else if (translationDiffBody[1] < 0) {
        vec2.set(translationForce, 0, -Math.pow(translationDiffBody[1], 2));
      }

      vec2.multiply(translationForce, translationForce, translationNormal);
      vec2.scaleAndAdd(
        icon.velocity,
        icon.velocity,
        translationForce,
        timeDelta * 0.0001,
      );
    }

    vec2.subtract(translationDiffMouse, icon.translation, mouse);

    if (
      Math.abs(translationDiffMouse[0]) < icon.radius &&
      Math.abs(translationDiffMouse[1]) < icon.radius
    ) {
      const distance = Math.min(
        0,
        vec2.length(translationDiffMouse) - icon.radius,
      );

      if (distance < 0) {
        vec2.set(
          translationForce,
          translationDiffMouse[0],
          translationDiffMouse[1],
        );
        vec2.normalize(translationForce, translationForce);
        vec2.scale(translationForce, translationForce, Math.pow(distance, 2));

        vec2.scaleAndAdd(
          icon.velocity,
          icon.velocity,
          translationForce,
          timeDelta * 0.001,
        );
      }
    }

    for (let j = i + 1; j < loadedIconsCount; j++) {
      const iconOther = loadedIcons[j];

      vec2.multiply(
        translationNormalizedOther,
        iconOther.translation,
        translationNormal,
      );
      vec2.subtract(
        translationDiffIcon,
        icon.translation,
        iconOther.translation,
      );

      if (
        Math.abs(translationDiffIcon[0]) < icon.radius + iconOther.radius &&
        Math.abs(translationDiffIcon[1]) < icon.radius + iconOther.radius
      ) {
        const distance = Math.min(
          0,
          vec2.length(translationDiffIcon) - icon.radius - iconOther.radius,
        );

        if (distance < 0) {
          vec2.set(
            translationForce,
            translationDiffIcon[0],
            translationDiffIcon[1],
          );
          vec2.normalize(translationForce, translationForce);
          vec2.scale(translationForce, translationForce, Math.pow(distance, 2));

          vec2.scaleAndAdd(
            icon.velocity,
            icon.velocity,
            translationForce,
            timeDelta * 0.0001,
          );

          translationForce[0] *= -1;
          translationForce[1] *= -1;

          vec2.scaleAndAdd(
            iconOther.velocity,
            iconOther.velocity,
            translationForce,
            timeDelta * 0.0001,
          );
        }
      }
    }

    const speed = 0.05 - vec2.length(icon.velocity);

    if (Math.abs(speed) > 0.01) {
      vec2.set(translationForce, icon.velocity[0], icon.velocity[1]);

      vec2.normalize(translationForce, translationForce);
      vec2.scaleAndAdd(
        icon.velocity,
        icon.velocity,
        translationForce,
        speed * timeDelta,
      );
    }

    vec3.add(icon.translation, icon.translation, icon.velocity);

    reflectionShadowIconData[i * 8 + 0] = icon.translation[0];
    reflectionShadowIconData[i * 8 + 1] = icon.translation[1];
    reflectionShadowIconData[i * 8 + 2] = icon.translation[2];
    reflectionShadowIconData[i * 8 + 3] = icon.reflectionColor[0];
    reflectionShadowIconData[i * 8 + 4] = icon.reflectionColor[1];
    reflectionShadowIconData[i * 8 + 5] = icon.reflectionColor[2];
    reflectionShadowIconData[i * 8 + 6] = icon.reflectionColor[3];

    if (icon.appearFinished !== true) {
      if (icon.appearTimeStart === null) {
        icon.appearTimeStart = time;
      }

      const appearTimeDelta = time - icon.appearTimeStart;
      let appearProgress = 0;
      if (appearTimeDelta >= appearDuration) {
        appearProgress = 1;

        icon.appearFinished = true;
      } else {
        appearProgress = appearTimeDelta / appearDuration;
      }

      icon.appearProgress = 1 - Math.pow(1 - appearProgress, 4);

      reflectionShadowIconData[i * 8 + 7] = icon.radius * icon.appearProgress;
    } else {
      reflectionShadowIconData[i * 8 + 7] = icon.radius;
    }
  }

  gl.clearColor(0, 0, 0, 0);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  gl.enable(gl.BLEND);
  gl.blendFunc(gl.ONE, gl.ONE);

  gl.disable(gl.DEPTH_TEST);

  gl.bindVertexArray(reflectionShadowVOA);

  gl.bindBuffer(gl.ARRAY_BUFFER, reflectionShadowIconsBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, reflectionShadowIconData, gl.DYNAMIC_DRAW);

  gl.useProgram(reflectionProgram);
  gl.uniformMatrix4fv(reflectionUniforms.projectionView, false, projectionView);

  gl.drawElementsInstanced(
    gl.TRIANGLES,
    reflectionShadowIndexCount,
    gl.UNSIGNED_SHORT,
    0,
    loadedIconsCount,
  );

  gl.useProgram(shadowProgram);
  gl.uniformMatrix4fv(shadowUniforms.projectionView, false, projectionView);

  gl.drawElementsInstanced(
    gl.TRIANGLES,
    reflectionShadowIndexCount,
    gl.UNSIGNED_SHORT,
    0,
    loadedIconsCount,
  );

  gl.disable(gl.BLEND);
  gl.enable(gl.DEPTH_TEST);
  gl.useProgram(shadedProgram);

  for (let i = 0; i < loadedIconsCount; i++) {
    const icon = loadedIcons[i];

    quat.set(
      rotation,
      Math.sin(icon.rotationFrequency[0] * time + icon.rotationOffset[0]) * 0.2,
      Math.sin(icon.rotationFrequency[1] * time + icon.rotationOffset[1]) * 0.2,
      Math.sin(icon.rotationFrequency[2] * time + icon.rotationOffset[2]) * 0.2,
      1,
    );

    quat.multiply(rotation, icon.rotation, rotation);

    if (icon.appearFinished === true) {
      vec3.set(scale, icon.scale[0], icon.scale[1], icon.scale[2]);
    } else {
      vec3.scale(scale, icon.scale, icon.appearProgress);
    }

    mat4.fromRotationTranslationScale(model, rotation, icon.translation, scale);
    gl.bindVertexArray(icon.VAO);
    gl.uniformMatrix4fv(shadedUniforms.model, false, model);
    gl.uniformMatrix4fv(shadedUniforms.projectionView, false, projectionView);
    gl.uniform1i(shadedUniforms.paletteSampler, 0);
    gl.uniform1i(shadedUniforms.diffuseSampler, 1);
    gl.uniform1i(shadedUniforms.glossySampler, 2);
    gl.drawElements(gl.TRIANGLES, icon.indexCount, gl.UNSIGNED_SHORT, 0);
  }

  requestAnimationFrame(handleAnimationFrame);
}

const resizeObserver = new ResizeObserver((entries) => {
  for (const entry of entries) {
    if (entry.target === bodyElement) {
      bodyWidth = entry.contentBoxSize[0].inlineSize;
      bodyHeight = entry.contentBoxSize[0].blockSize;
      updateProjectionView();
      updateCanvas();
    } else if (entry.target === mainElement) {
      mainWidth = entry.contentBoxSize[0].inlineSize;
      mainHeight = entry.contentBoxSize[0].blockSize;
      mainRadius = Math.max(mainWidth, mainHeight) * 0.5;
    } else if (entry.target === iconScaleElement) {
      iconScale = entry.contentBoxSize[0].inlineSize / 100;

      for (let i = 0; i < loadedIconsCount; i++) {
        const icon = loadedIcons[i];
        vec3.scale(icon.scale, icon.size, iconScale);
        icon.radius = vec3.length(icon.scale) * 128;
        icon.translation[2] = iconScale * 34;
      }
    }
  }
});

resizeObserver.observe(bodyElement);
resizeObserver.observe(mainElement);
resizeObserver.observe(iconScaleElement);

icons.forEach(async (icon) => {
  const geometry = await loadGeometry(icon.geometryUrl);

  icon.VAO = gl.createVertexArray();

  icon.appearTimeStart = null;
  icon.appearFinished = false;

  icon.size = geometry.size;
  icon.scale = vec3.create();
  vec3.scale(icon.scale, icon.size, iconScale);

  icon.radius = vec3.length(icon.scale) * 256;

  icon.reflectionColor = geometry.reflectionColor;
  icon.translation = vec3.fromValues(
    Math.random() * 2 - 1,
    Math.random() * 2 - 1,
    iconScale * 34,
  );
  icon.translationForce = vec2.create();
  icon.velocity = vec3.create();
  vec2.random(icon.velocity, 0.2);
  icon.velocity[2] = 0;

  icon.rotation = geometry.rotation;
  icon.rotationFrequency = vec3.fromValues(
    0.0001 * Math.random() * Math.PI * 2,
    0.0001 * Math.random() * Math.PI * 2,
    0.00005 * Math.random() * Math.PI * 2,
  );
  icon.rotationOffset = vec3.fromValues(
    Math.random() * 1000,
    Math.random() * 1000,
    Math.random() * 1000,
  );
  icon.indexCount = geometry.indexCount;

  gl.bindVertexArray(icon.VAO);

  const elementArrayBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, elementArrayBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, geometry.arrayBuffer, gl.STATIC_DRAW);

  const geometryBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, geometryBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, geometry.arrayBuffer, gl.STATIC_DRAW);

  gl.enableVertexAttribArray(0);
  gl.vertexAttribPointer(0, 3, gl.BYTE, false, 0, geometry.coordsOffset);

  gl.enableVertexAttribArray(1);
  gl.vertexAttribPointer(1, 3, gl.BYTE, false, 0, geometry.normalsOffset);

  gl.enableVertexAttribArray(2);
  gl.vertexAttribPointer(2, 1, gl.UNSIGNED_BYTE, false, 0, geometry.uvsOffset);

  loadedIcons.push(icon);
  loadedIconsCount = loadedIcons.length;
});

Promise.all([
  loadImage(palette),
  loadImage(diffuse),
  loadImage(glossy),
  loadGeometry(circle),
]).then(([palette, diffuse, glossy, circle]) => {
  reflectionShadowIndexCount = circle.indexCount;

  gl.bindVertexArray(reflectionShadowVOA);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, reflectionShadowElementArrayBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, circle.arrayBuffer, gl.STATIC_DRAW);

  gl.bindBuffer(gl.ARRAY_BUFFER, reflectionShadowArrayBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, circle.arrayBuffer, gl.STATIC_DRAW);

  gl.enableVertexAttribArray(0);
  gl.vertexAttribDivisor(0, 0);
  gl.vertexAttribPointer(0, 3, gl.BYTE, false, 0, circle.coordsOffset);

  gl.enableVertexAttribArray(1);
  gl.vertexAttribDivisor(1, 0);
  gl.vertexAttribPointer(1, 1, gl.UNSIGNED_BYTE, false, 0, circle.uvsOffset);

  gl.bindBuffer(gl.ARRAY_BUFFER, reflectionShadowIconsBuffer);

  gl.enableVertexAttribArray(2);
  gl.vertexAttribDivisor(2, 1);
  gl.vertexAttribPointer(2, 3, gl.FLOAT, false, 8 * 4, 0);

  gl.enableVertexAttribArray(3);
  gl.vertexAttribDivisor(3, 1);
  gl.vertexAttribPointer(3, 4, gl.FLOAT, false, 8 * 4, 3 * 4);

  gl.enableVertexAttribArray(4);
  gl.vertexAttribDivisor(4, 1);
  gl.vertexAttribPointer(4, 1, gl.FLOAT, false, 8 * 4, 7 * 4);

  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, paletteTexture);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, palette);

  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

  gl.activeTexture(gl.TEXTURE1);
  gl.bindTexture(gl.TEXTURE_2D, diffuseTexture);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, diffuse);

  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

  gl.activeTexture(gl.TEXTURE2);
  gl.bindTexture(gl.TEXTURE_2D, glossyTexture);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, glossy);

  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

  requestAnimationFrame(handleAnimationFrame);
});
